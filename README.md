# Ansible Role: Prometheus
An ansible role which setup Prometheus Server for your awesome monitoring setup.

## Requirements
- Python3 on target machine
- If u have mutliple python version installed in your target machine then run python3 as interpreter

This supports below listed OS platforms
- **RedHat** or **CentOS** 6 and 7 and 8
- **Debian**

## Role Variables
The role variables are defined in the Default section of the role. So there is not so many variables you just have to pass the prometheus version.

### Mandatory Variables
There is no mandatory variables needed.

### Optional Variables

|**Variable**|**Default Value**|**Description**|
|------------|-----------------|---------------|
|version|2.12.0|Prometheus server version|
|prometheus_ip|0.0.0.0|Default IP which binds with Prometheus|
|prometheus_port|9090|Default port number on which Prometheus listens|

## Dependencies
None :-)

## Example Playbook
Here is an example playbook for running this role
```yaml
---
- hosts: prometheus
  user: test-user
  become: true
  roles:
    - prometheus_role
```
For inventory you can create a host file in which you can define your server ip, For example:-
```ini
[prometheus]
10.1.1.100
```

You can simply use this role by using this command
```shell
ansible-playbook -i hosts -e ansible_python_interpreter=/usr/bin/python3 site.yml
```
In case if throws error in running yum module run with providing extra vars ansible_python_interpreter=/usr/bin/python3
## License

GNU

## Author Information

This role is written and maintained by [Bimlesh Kumar Singh]. If you have any queries and sugesstion, please feel free to reach.

